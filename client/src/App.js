// React
import React,{ useState } from "react";

// Bulma
import "react-bulma-components/dist/react-bulma-components.min.css";

// React Router
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

// Apollo
import ApolloClient from "apollo-boost";
import {ApolloProvider} from "react-apollo";

// Components
import Team from "./components/Team";
import NavBar from "./components/NavBar";
import Task from "./components/Task";
import Member from "./components/Member";
import UpdateMember from './components/UpdateMember'
import UpdateTeam from './components/UpdateTeam'
import UpdateTask from './components/UpdateTask'
import Login from './components/Login'


// create an instance to all our GraphQL components

const client = new ApolloClient( {uri:"http://localhost:3001/batch43"} )



function App() {
  const [username, setUserName] = useState(localStorage.getItem('username'));
  const [position, setPosition] = useState(localStorage.getItem('position'));
  
  let firstName = localStorage.getItem('firstName')
  
  const updateSession = () => {
    setUserName(localStorage.getItem('username'))
    setPosition(localStorage.getItem('position'))
  }

  const Logout = () => {
    localStorage.removeItem('username')
    localStorage.removeItem('position')
    localStorage.removeItem('firstName')
    updateSession()
    return <Redirect to="/login" />
  }

  const loggedUser = props => <Login updateSession={updateSession} />
  
  
  return (
    <ApolloProvider client={client}>
      <BrowserRouter>
        <div>
          <NavBar firstName={firstName}/>
          <Switch>
            <Route exact path="/" component={Member} />
            <Route path="/tasks" component={Task} />
            <Route path="/teams" component={Team} />
            <Route path="/navbar" component={NavBar} />
            <Route path="/members/update/:id" component={UpdateMember} />
            <Route path="/teams/update/:id" component={UpdateTeam} />
            <Route path="/tasks/update/:id" component={UpdateTask} />
            <Route path="/members/login" render={loggedUser}  /> {/* render para mag pasa ng props */}
            <Route path="/members/logout" component={Logout} />
          </Switch>
        </div>
      </BrowserRouter>
    </ApolloProvider>
  );
}

export default App;
