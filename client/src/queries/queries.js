import {gql} from "apollo-boost"

const getMembersQuery = gql`
	{
	  getMembers{
	  	id
	    firstName
	    lastName
	    position
	    username
	    password
	    teamid
	    teams{
	      name
	      tasks {
	        description
	      }
	    }
	  }
	}
`;

const getTasksQuery = gql`
	{
	  getTasks{
	    description
	    teamId
	    isCompleted
	    id
	    teams{
	      name
	    }
	  }
	}
`;

const getTeamsQuery = gql`
	{
	  getTeams{
	    name
	    id
	  }
	}
`;

export {getMembersQuery, getTasksQuery, getTeamsQuery} ;