import {gql} from "apollo-boost";


// Create
const createMemberMutation = gql`
    mutation (
        $firstName: String!
        $lastName : String!
        $teamid : String!
        $position : String!
        $username: String!
        $password: String!
    ) {
        createMember(
            firstName: $firstName,
            lastName: $lastName, 
            position: $position,
            teamid: $teamid,
            username: $username,
            password: $password
        ){
            id
            firstName
            lastName
            position
            teamid
            username
            password
        }
    }
`;

const createTeamMutation = gql`
    mutation(
        $name: String!
    ) {
        createTeam(
            name: $name
        ){
            id
            name
        }
    }
`;

const createTaskMutation = gql`
    mutation(
        $description: String!
        $teamId : String!
    ) {
        createTask(
            description : $description
            teamId : $teamId
            isCompleted : "Not Accomplished"
        ) {
            id
            description
            isCompleted
            teamId
        }
    }
`;
// Update

const updateMemberMutation = gql`
    mutation(
        $id : ID!
        $firstName: String!
        $lastName: String!
        $teamid : String!
        $position: String!
    ) {
        updateMember(
            id: $id
            firstName: $firstName
            lastName: $lastName
            position: $position
            teamid: $teamid
        ) {
            id
            firstName
            lastName
            position
            teamid
        }
    }
`;

// Delete
const deleteMemberMutation = gql`
    mutation($id: String!) {
        deleteMember(id: $id)
    }
`;

const deleteTeamMutation = gql`
    mutation($id: String!) {
        deleteTeam(id: $id)
    }
`;

const deleteTaskMutation = gql`
    mutation($id: String!) {
        deleteTask(id: $id)
    }
`;

// Login

const logInMutation = gql`
    mutation(
    $username: String!
    $password: String!
    ) {
      logInMember(
      username: $username 
      password: $password
      ) {
        username
        password
        firstName
        lastName
      }
    }
`;

export { 
    createMemberMutation, 
    createTeamMutation, 
    createTaskMutation, 
    updateMemberMutation,
    deleteMemberMutation, 
    deleteTeamMutation, 
    deleteTaskMutation,
    logInMutation
};