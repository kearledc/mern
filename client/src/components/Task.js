import React from "react";
import { Link } from 'react-router-dom'

import { Container, Columns, Card, Button } from "react-bulma-components";
// import Query
import { getTasksQuery, getTeamsQuery } from '../queries/queries';
import { createTaskMutation, deleteTaskMutation } from '../queries/mutations';
// import GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// CSS
import '../App.css';
// Swal
import Swal from "sweetalert2";

const Task = props => {
	const taskData = props.getTasksQuery.getTasks? props.getTasksQuery.getTasks:[]
	const teamData = props.getTeamsQuery.getTeams? props.getTeamsQuery.getTeams:[]
	
	// Input of New Task
	const onFormSubmit = e => {
		// Inputing the values of New Task
		let description = e.target.taskDescription.value.trim()
		let teamId = e.target.teamId.value
		// Pushing the new task value to the objects
		let newTask = {
			description,
			teamId
		}
		console.log(newTask)
		// Inputting the newTask to the database
		props.createTaskMutation({
			variables: newTask,
			refetchqueries: [{
				query: getTasksQuery
			}]
		})
	}
	
	// Delete of Task
	const deleteTaskHandler = e => {
		let id = e.target.id

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteTaskMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getTasksQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The member has been deleted.",
					"success"
				);
			}
		});
	}

	return (
		<Container>
			<hr />
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Task</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={onFormSubmit}>
								<div className="field">
									<label
										className="label"
										htmlFor="taskDescription"
									>
										Description
									</label>
									<input
										id="taskDescription"
										className="input"
										type="text"
										name="taskDescription"
									/>
								</div>

								<div className="field">
									<label className="label" htmlFor="teamName">
										Team
									</label>
									<div className="control">
										<div className="select">
											<select name="teamId">
											<option disabled selected >Select  A Team</option>
											{teamData.map(team => <option value={team.name} key={team.id}>{team.name}</option>					
											)}
											</select>											
										</div>
									</div>
								</div>

								<Button type="submit" color="success" fullwidth>
									Add New Task
								</Button>
							</form>
						</Card.Content>
					</Card>

					<img src="/images/smile.gif" alt="smileandwave"/>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Task List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is bordered">
									<thead>
										<th>Description</th>
										<th>Teams</th>
										<th>Accomplished</th>
										<th></th>
										<th></th>
									</thead>
									<tbody>
										{taskData.map(task => {
											return(
										<tr key={task.id}>
											<td>{task.description}</td>
											<td>{task.teamId}</td>
											<td>{task.isCompleted}</td>

											<td>
											<Link to={`/tasks/update/ ${task.id}`}>
												<Button color="link" fullwidth outlined={true}>
													Update
												</Button>
											</Link>
											</td>
											<td>
												<Button
													color="danger"
													fullwidth
													outlined={true}
													id={task.id}
													onClick={deleteTaskHandler}
												>
													Delete
												</Button>
											</td>
										</tr>
											)
										})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getTeamsQuery, {name: "getTeamsQuery"}),
	graphql(getTasksQuery, {name: "getTasksQuery"}),
	graphql(createTaskMutation, {name: "createTaskMutation"}),
	graphql(deleteTaskMutation, {name: "deleteTaskMutation"})
)(Task);
