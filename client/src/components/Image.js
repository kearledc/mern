import React from "react";

const Image = props => <img src={props.img} alt="Background" />;

export default Image;
