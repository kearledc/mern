import React, { useState } from 'react';
import { Link, Redirect } from "react-router-dom";
import { graphql } from 'react-apollo'


//Components
import { getMembersQuery } from '../queries/queries'
import { logInMutation } from '../queries/mutations'
// Lodash
import { flowRight as compose } from 'lodash'
// Bulma
import { Section, Heading, Card, Columns, Container, Box } from 'react-bulma-components';
// Swal
import Swal from "sweetalert2";

const Login = props => {
	console.log(props)
	let login = props.logInMutation? props.logInMutation : [];
	const [logInSuccess, setLoginSuccess] = useState(false);

	const onFormSubmit = e => {
		e.preventDefault()
		console.log('hello from submit')
		let username = e.target.username.value
		let password = e.target.password.value
		login({
			variables: {
				username,
				password
			}
		})
		.then(res => {
			// console.log(res.data.logInMember)
			let data = res.data.logInMember
			// console.log(data)

			if(data === null){
				Swal.fire({
					title: 'Nice Try Kid',
					text: 'Better Luck Next Time',
					type: 'error'
				})
			}
			else{
				localStorage.setItem('username' , data.username)
				localStorage.setItem('password' , data.password)
				localStorage.setItem('firstName' , data.firstName)
				setLoginSuccess(true)
				props.updateSession()
				Swal.fire({
					title: 'Welcome Skipper!',
					text: 'My Analysis is',
					type: 'success'
				})
			}
		})
	}
	
	if(!logInSuccess){
		console.log('Something Went Wrong')
	}
	else{
		console.log('Good')
		return <Redirect to="/" />
	}

	return(
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Member Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={onFormSubmit}>
								<label htmlFor="username">Username</label>
									<input type="text" name="username" className="input" placeholder="Please Input Username"/>
								<label htmlFor="password">Password</label>
									<input type="password" name="password" className="input" placeholder="Please Input Password"/>
								<button className="button is-success is-fullwidth">
									Login
								</button>
							</form>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	)
}

export default compose(
	graphql(getMembersQuery, {name: "getMembersQuery"}),
	graphql(logInMutation, {name: "logInMutation"})
)(Login);