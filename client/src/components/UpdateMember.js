import React from 'react';
import { Link } from 'react-router-dom'
// Bulma
import { Section, Heading, Card, Columns, Container } from 'react-bulma-components';
// Queries
import { getTeamsQuery } from '../queries/queries'
import { updateMemberMutation } from '../queries/mutations'
// Lodash
import { flowRight as compose } from 'lodash'
// GraphQL
import { graphql } from 'react-apollo'

const UpdateMember = props => {
	console.log(props)
	const teamData = props.getTeamsQuery.getTeams ? props.getTeamsQuery.getTeams : []
	// console.log(teamData)

	
	const onFormSubmit = e => {
		e.preventDefault()
		let firstName = e.target.firstName.value.trim()
		let lastName = e.target.lastName.value.trim()
		let position = e.target.position.value.trim()
		let teamid = e.target.teamid.value

		let updatedMember = {
			firstName,
			lastName,
			position,
			teamid
		}
		
		props.updateMemberMutation({
			variables: updatedMember
		})
	}
	
	return(
		<Container>
			<Columns>
				<Columns.Column size="half" offset="one-quarter">
					<Card>
						<Card.Header>
							<Card.Header.Title>
								Member Details
							</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={onFormSubmit}>
								<label htmlFor="firstName">Enter First Name</label>
									<input type="text" name="firstName" className="input"/>
								<label htmlFor="lastName">Enter Last Name</label>
									<input type="text" name="lastName" className="input"/>
								<label htmlFor="position">Enter The Position</label>
									<input type="text" name="position" className="input"/>
								<label htmlFor="team">Choose Your Team</label>
									<div className="select is-fullwidth">
										<select name="teamid">
											<option disabled selected>Select Team</option>
											{teamData.map(team => 
												<option value={team.name} key={team.id}>{team.name}</option>
											)}
										</select>
									</div>
								<button className="button is-success is-fullwidth">
									Update
								</button>
							</form>
								<Link>
									<button type="button" className="button is-danger is-fullwidth">
										Cancel
									</button>
								</Link>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	)
}

export default compose(
	graphql(getTeamsQuery, {name: "getTeamsQuery"}),
	graphql(updateMemberMutation, {name: "updateMemberMutation"})
)(UpdateMember);