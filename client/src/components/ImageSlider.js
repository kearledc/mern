import React, { useState } from "react";
import "./../App.css";
import { Button, Box } from "react-bulma-components";

const ImageSlider = () => {
	const images = [
		"/images/girl4.jpg",
		"/images/girl6.jpg",
		"/images/girl7.jpg"
	];

	const [index, setIndex] = useState(0);

	const prevClick = () => {
		index === 0
			? setIndex(index + (images.length - 1))
			: setIndex(index - 1);
	};

	const nextClick = () => {
		index === images.length - 1 ? setIndex(0) : setIndex(index + 1);
	};

	// const nextImg = () => {
	// 	index === images.length - 1 ? setIndex(0) : setIndex(index + 1);
	// };

	// setInterval(nextImg, 2000);
	return (
		<div className="App-Slider">
			<h1>Choose your Masseuse</h1>
			<img src={images[index]} alt="girlImage" />
			<Box>
				<Button
					color="dark"
					outlined={true}
					onClick={prevClick}
					className="prevButton"
				>
					Prev
				</Button>
				<Button
					color="dark"
					outlined={true}
					onClick={nextClick}
					className="nextButton"
				>
					Next
				</Button>
			</Box>
		</div>
	);
};

export default ImageSlider;
