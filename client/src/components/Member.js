import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

// Bulma
import { Container, Columns, Card, Button } from "react-bulma-components";
// Query
import { getMembersQuery, getTeamsQuery } from '../queries/queries'
import { createMemberMutation, deleteMemberMutation } from '../queries/mutations'
// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// CSS
import '../App.css';
// Swal
import Swal from "sweetalert2";

const Member = props => {
	// console.log(props)
	const onFormSubmit = e => {
		// e.preventDefault()
		let firstName = e.target.firstName.value.trim()
		let lastName = e.target.lastName.value.trim()
		let position = e.target.position.value.trim()
		let username = e.target.username.value.trim()
		let password = e.target.password.value.trim()
		let teamid = e.target.teamid.value
		
		let newMember = {
			firstName,
			lastName,
			position,
			username,
			password,
			teamid
		};
		
		console.log(newMember)
		props.createMemberMutation({
			variables : newMember,
			refetchQueries: [{
				query : getMembersQuery
			}]
		})		
	}

	const memberData = props.getMembersQuery.getMembers ? props.getMembersQuery.getMembers : [];

	const teamData = props.getTeamsQuery.getTeams ? props.getTeamsQuery.getTeams : [];

	let loadingMessage = props.getMembersQuery.loading ? <div className="control is-loading">Fetching Members..</div> : "";
	
	const deleteMemberHandler = e => {
		console.log("deleting a member...");
		console.log(e.target.id);
		let id = e.target.id;

		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteMemberMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getMembersQuery
						}
					]
				});
				Swal.fire(
					"Deleted!",
					"The member has been deleted.",
					"success"
				);
			}
		});
	}


	return (
		<Container>
			<hr />
			<Columns className="is-centered">
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Member</Card.Header.Title>
						</Card.Header>

						<Card.Content>
							<form onSubmit={onFormSubmit}>
								<div className="field">
									<label htmlFor="firstName" className="label">
										First Name
									</label>
									<input type="text" id="firstName" className="input" name="firstName" />
								</div>

								<div className="field">
									<label htmlFor="lastName" className="label">
										Last Name
									</label>
									<input type="text" id="lastName" className="input" name="lastName" />
								</div>

								<div className="field">
									<label htmlFor="position" className="label">
										Position
									</label>
									<input type="text" id="position" className="input" name="position" />
								</div>

								<div className="field">
									<label htmlFor="username" className="label">
										Username
									</label>
									<input type="text" id="username" className="input" name="username" />
								</div>

								<div className="field">
									<label htmlFor="password" className="label">
										Password
									</label>
									<input type="password" id="password" className="input" name="password" />
								</div>

								<div className="field">
									<label htmlFor="teamid" className="label">
										Team
									</label>
									<div className="control">
										<div className="select">
											<select name="teamid" id="">
											<option selected disabled>Select Team</option>
											{teamData.map(team => 
												<option value={team.name} key={team.id}>
													{team.name}
												</option>)}
											</select>
										</div>	
									</div>
								</div>
								<Button type="submit" color="success" fullwidth>
									Add New Member
								</Button>
							</form>
						</Card.Content>
					</Card>
					<img src="/images/high5.gif" alt=""/>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card.Header>
						<Card.Header.Title>Member List</Card.Header.Title>
					</Card.Header>

					<Card.Content>
						<div className="table-container">
							<table className="table is-fullwidth is-bordered">
							{loadingMessage}
								<thead>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Position</th>
									<th>Team Name</th>
									<th colspan={2}>Action</th>
								</thead>
								<tbody>
									{memberData.map(member => {
										let team = member.teams
										return(

									<tr key={member.id}>
										<td>{member.firstName}</td>
										<td>{member.lastName}</td>
										<td>{member.position}</td>										
										<td>
										{team? team.name : "This Guy Has No Team Skipper"}
										</td>										
										<td>
										<Link to={`/members/update/ ${member.id}`}>
											<Button color="link" fullwidth outlined={true}>
												Update
											</Button>
										</Link>	
										</td>
										<td>	
											<Button color="danger" id={member.id} fullwidth outlined={true} onClick={deleteMemberHandler}>
												Delete
											</Button>
										</td>
									</tr>
										)
									})}
								</tbody>
							</table>
						</div>
					</Card.Content>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getMembersQuery, { name: "getMembersQuery" }),
	graphql(getTeamsQuery, { name: "getTeamsQuery" }),
	graphql(createMemberMutation, { name: "createMemberMutation" }),
	graphql(deleteMemberMutation, { name: "deleteMemberMutation" })
)(Member);
