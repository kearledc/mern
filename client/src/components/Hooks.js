import React, { useState } from "react";
import Counter from "./Counter";
import Form from "./Form";
import "./../App.css";

const Hooks = () => {
	let [email, setEmail] = useState("");
	const emailSubmit = e => {
		// e.preventDefault();
		setEmail(`Your email is ${e.target.value}`);
	};

	return (
		<div className="App-header">
			<Counter />
			<h1>Hello: {email}</h1>
			<input type="text" onChange={emailSubmit} />
			<Form />
		</div>
	);
};

export default Hooks;
