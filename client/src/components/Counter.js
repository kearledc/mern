import React, { useState } from "react";

const Counter = () => {
	let [count, setCount] = useState(0);

	const addOne = () => {
		setCount(count++);
	};

	const minusOne = () => {
		setCount(count--);
	};

	const reset = () => {
		setCount((count = 0));
	};

	return (
		<div>
			<h1>Count: {count}</h1>
			<button onClick={addOne}> + </button>
			<button onClick={minusOne}> - </button>
			<button onClick={reset}>Reset</button>
		</div>
	);
};

export default Counter;
