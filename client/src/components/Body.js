import React from "react";
import logo from "./../logo.svg";
import "./../App.css";

const Body = () => <p>Hello from Body</p>;

const Body2 = () => <p>Hello from Body2</p>;

// multiple components to export? explicitly define each component

export { Body, Body2 };
