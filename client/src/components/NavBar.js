import React from "react";
import { Navbar } from "react-bulma-components";
import { Link } from "react-router-dom";
// CSS
import '../App.css'

const NavBar = props => {
	console.log(props)
	let userOrGuest = props.firstName
		? `Hello! ${props.firstName}`
		: "Hello Guest";
	
	let logInAndOut = props.firstName? 
	<Link className="navbar-item" to="/members/logout">
		Logout
	</Link> 
	: 
	<Link className="navbar-item" to="/members/login">
		Login
	</Link>


	return (
		<Navbar color="black">
			<Navbar.Brand>
				<Link to="/" className="navbar-item logo">
					<strong>Madagascar</strong>
				</Link>
			</Navbar.Brand>

			<Navbar.Menu>
				<Navbar.Container position="end">
					<Link className="navbar-item" to="/">
						Members
					</Link>
					<Link className="navbar-item" to="/teams">
						Teams
					</Link>
					<Link className="navbar-item" to="/tasks">
						Tasks
					</Link>
					<Link className="navbar-item" to="">
						{userOrGuest}
					</Link>
						{logInAndOut}
					
				</Navbar.Container>
			</Navbar.Menu>
		</Navbar>
	);
};

export default NavBar;
