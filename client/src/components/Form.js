import React, { useState } from "react";

let Form = () => {
	let [password, passRev] = useState("");

	const passwordReveal = e => {
		passRev(e.target.value);
	};

	return (
		<div>
			<br />
			<h1>Your Password is {password}</h1>
			<input type="password" onChange={passwordReveal} />
		</div>
	);
};

export default Form;
