import React from "react";
import { Link } from 'react-router-dom'
// Bulma
import {
	Container,
	Card,
	Columns,
	Button
} from "react-bulma-components";

// Query
import {getTeamsQuery} from '../queries/queries';
import {createTeamMutation, deleteTeamMutation} from '../queries/mutations';

// GraphQL
import {graphql} from 'react-apollo';
// Lodash
import { flowRight as compose } from 'lodash';
// CSS
import '../App.css'
// Swal
import Swal from "sweetalert2";

const Team = props => {
	// console.log(props)

	// let data = props.data;
	let teamData = props.getTeamsQuery.getTeams? props.getTeamsQuery.getTeams : [];
	
	let onFormSubmit = e => {
		// e.preventDefault()
		let teamName = e.target.teamName.value.trim()

		let newTeam = {
			name: teamName
		}
		
		// console.log(newTeam);
		props.createTeamMutation({
			variables: newTeam,
			refetchqueries: [{
				query: getTeamsQuery
			}]
		})
	}
	
	const deleteTeamHandler = e => {
		// console.log(e.target.id)
		let id = e.target.id
		Swal.fire({
			title: "Are you sure you want to delete?",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes, delete it!"
		}).then(result => {
			if (result.value) {
				props.deleteTeamMutation({
					variables: { id: id },
					refetchQueries: [
						{
							query: getTeamsQuery
						}
					]
				});
				Swal.fire(
					"Erased!",
					"The Team has been Destroyed!.",
					"success"
				);
			}
		});
	}

	return (
		<Container>
			<hr />
			<Columns>
				<Columns.Column size={3}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Add Team</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<form onSubmit={onFormSubmit}>
								<div className="field">
									<label className="label" htmlFor="teamName">
										Team Name
									</label>
									<input
										id="teamName"
										className="input"
										type="text"
										name="teamName"
									/>
								</div>

								<Button type="submit" color="success" fullwidth>
									Add New Team
								</Button>
							</form>
						</Card.Content>
					</Card>
					<img src="/images/you.gif" alt="you"/>
				</Columns.Column>

				<Columns.Column size={9}>
					<Card>
						<Card.Header>
							<Card.Header.Title>Team List</Card.Header.Title>
						</Card.Header>
						<Card.Content>
							<div className="table-container">
								<table className="table is-fullwidth is bordered">
									<thead>
										<th>Teams</th>
										<th>Action</th>
										<th></th>
									</thead>
									<tbody>
									{teamData.map(team => {
										return(

										<tr>
											<td key={team.id}>{team.name}</td>

											<td>
											<Link to={`/teams/update/ ${team.id}`}>
												<Button color="link" fullwidth outlined={true}>
													Update
												</Button>
											</Link>	
											</td>
											<td>
												<Button
													id={team.id}
													color="danger"
													fullwidth outlined={true}
													onClick={deleteTeamHandler}
												>
													Delete
												</Button>
											</td>
										</tr>
										)
									})}
									</tbody>
								</table>
							</div>
						</Card.Content>
					</Card>
				</Columns.Column>
			</Columns>
		</Container>
	);
};

export default compose(
	graphql(getTeamsQuery, {name: "getTeamsQuery"}),
	graphql(createTeamMutation, {name: "createTeamMutation"}),
	graphql(deleteTeamMutation, {name: "deleteTeamMutation"})
)(Team);
