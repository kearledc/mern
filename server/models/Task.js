const mongoose = require('mongoose')
const Schema = mongoose.Schema // Use Mongoose' Schema as our basis

// Actual Schema
const taskSchema = new Schema({
	description: String,
	teamId: String,
	isCompleted: String },
	{
		timestamps: true
	
})

//export the model as a module
module.exports = mongoose.model('Task', taskSchema); // this creates a Model called Task using the schema task
