const mongoose = require('mongoose')
const Schema = mongoose.Schema 


const itemSchema = new Schema({
		name: {
			type: String,
			required: [true, 'Enter a Name']
		},
		price: {
			type: Number,
			required: [true, 'Enter a Price']
		},
		quantity: {
			type: Number,
			required: [true, 'Enter a Quantity']
		},
		teamid: {
			type: String
		}


	},
	{
		timestamps: true
	}
)

//export the model as a module
module.exports = mongoose.model('Item', itemSchema); 
