const mongoose = require('mongoose')
const Schema = mongoose.Schema 


const teamSchema = new Schema({
	name: {
		type: String,
		required: [true, 'Enter a Name']
	}},
	{
		timestamps: true
	}
)

//export the model as a module
module.exports = mongoose.model('Team', teamSchema); 
