const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const memberSchema = new Schema({
		firstName : {
			type: String,
			required: [true, 'Enter a First Name']
		},
		lastName : {
			type: String,
			required: [true, 'Enter a Last Name']
		},
		position : {
			type: String,
			required: [true, 'Enter a Position']
		},
		teamid : {
			type: String,
			required: true
		},
		username : {
			type: String,
			required: [true, 'Enter a username']
		},
		password : {
			type: String,
			required: [true, 'Enter a Password']
		}
	},
	{
		timestamps: true
	}
)

module.exports = mongoose.model('Member', memberSchema);
