const express = require('express');
const mongoose = require('mongoose');
const app = express();

mongoose.connect('mongodb+srv://bigAdmin:admin123@bigandhuge-158vv.mongodb.net/BigAndHuge?retryWrites=true&w=majority', 
	{useNewUrlParser:true,
	useUnifiedTopology: true,
	useCreateIndex: true}
)

// mongoose.connect('mongodb://localhost:27017/BigAndHuge_DB', {useNewUrlParser:true})

mongoose.connection.once('open', () => {
	console.log('Welcome Back!')
})

// app.use(express.json());


// let member_route = require('./routes/member_routes.js')
// app.use('/members', member_route)

// let team_route = require('./routes/team_routes.js')
// app.use('/teams', team_route);

// let task_route = require('./routes/task_routes.js')
// app.use('/tasks', task_route);



const server = require('./queries/queries');

// the app will be served by the Apollo server instead of express
server.applyMiddleware({
	app,
	path: "/batch43"
})



const port = 3001;

app.listen(port, () => {
	console.log(`I love you Port ${port}`)
})

