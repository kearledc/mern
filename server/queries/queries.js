const {ApolloServer, gql} = require('apollo-server-express');
const bcrypt = require('bcrypt');

// mongoose models

const Member = require('../models/Member');
const Task = require('../models/Task');
const Team = require('../models/Team');
const Item = require('../models/Item')

const typeDefs = gql`
	# this is a comment
	# the type Query is the root of all GraphQL queries
	# used for executing "GET"
	# ! will require set parameters

	
	type TaskType {
		id : ID
		description : String
		teamId : String
		isCompleted : String
		teams : [TeamType]
	}

	type TeamType {
		id : ID
		name : String
		tasks: [TaskType]
	}

	type MemberType {
		id : ID
		firstName: String
		lastName: String
		position: String
		username: String
		password: String
		teamid: String
		teams: TeamType
	}

	type ItemType {
		id : ID
		name : String
		price : Int
		quantity : Int
		teamid : String
		teams: [TeamType]
	}

	# to use in the graphQL 
	type Query {
		#create a query called hello that will expect a string data type
		hello : String!
		getTeams : [TeamType]
		getTasks : [TaskType]
		getMembers : [MemberType]
		getItems : [ItemType]
		getMember(id: ID) : MemberType
		getTask(id: ID!) : TaskType
		getTeam(id: ID!) : TeamType 
	}

	#CUD functionality
	#we are mutating the server/database

	type Mutation {
		createTeam(name: String!) : TeamType
		createTask(
			description: String!
			teamId: String!
			isCompleted: String
		) : TaskType
		createMember(
			firstName: String!
			lastName: String!
			position: String!
			teamid: String!
			username: String!
			password: String!
		) : MemberType
		createItem(
			name: String!
			price: Int
			quantity: Int
			teamid: String!
		) : ItemType
		updateTeam(
			id : ID!
			name: String
		) : TeamType
		updateTask(
			id : ID!
			teamId: String
			description: String
			isCompleted: String
		) : TaskType
		updateMember(
			id : ID!
			firstName: String
			lastName: String
			position: String
		) : MemberType
		updateItem(
			id : ID!
			name: String
			price: Int
			quantity: Int
			teamid: String
		) : ItemType
		deleteTeam(
			id:String
		) : Boolean
		deleteMember(
			id:String
		) : Boolean
		deleteTask(
			id:String
		) : Boolean
		logInMember(
			username: String
			password: String
		) : MemberType
	}
`;

const resolvers = {
	// what are we going to do when the query is executed
	Query: {
		hello : () => 'my first query',
		
		getTeams : () => {
			return Team.find({})
		},

		getTasks : () => {
			return Task.find({})
		},

		getMembers : () => {
			return Member.find({})
		},

		getItems : () => {
			return Item.find({})
		},

		getMember : (_, args) => {
			// console.log(args.id);
			// console.log('executed get member query')
			return Member.findById(args.id)
		},

		getTask : (_,args) => {
			return Task.findById(args.id)
		},
		getTeam : (_,args) => {
			return Team.findById(args.id)
		}
	},

	Mutation : {
		createTeam : (_, args) => {
			// console.log(args);

			let newTeam = Team({
				name: args.name
			})

			// console.log(newTeam);

			return newTeam.save();
		},

		createTask : (_, {description, teamId}) => {
			let newTask = Task({
				description,
				teamId,
				isCompleted: "Not Accomplished"
			})

			return newTask.save();
		},

		createMember : (_,{firstName, lastName, position, teamid, username, password}) => {
			let newMember = Member({
				firstName,
				lastName, 
				position,
				teamid,
				username,
				password: bcrypt.hashSync(password, 10)
			})
			return newMember.save()
		},

		createItem : (_, {name, price, quantity, teamid}) => {
			let newItem = Item({
				name,
				price,
				quantity,
				teamid
			})

			return newItem.save();
		},

		updateTeam : (_, {id, name}) => {
			// console.log(args)
			return Team.findByIdAndUpdate(id, {
				$set: {
					name
				}
			})
			
		},

		updateTask : (_, {id, isCompleted}) => {
			return Task.findByIdAndUpdate(id, {
				$set: {
					isCompleted
				}
			})
		},

		updateMember : (_, {id, firstName, lastName, position, teamid}) => {
			return Member.findByIdAndUpdate(id, {
				$set: {
					firstName,
					lastName,
					position,
					teamid,
					username,
					password: bcrypt.hashSync(password, 10)
				}
			})
		},
		
		updateItem : (_, {id, name, price, quantity, teamid}) => {
			return Item.findByIdAndUpdate( id, {
				$set: {
					name,
					price,
					quantity,
					teamid
				}
			})
		},

		deleteTeam : (_,{id}) => {
			return Team.findByIdAndDelete(id, (err, team) => {
				if(!team){
					console.log('No Team Found')
					return false
				}
				console.log('Team Deleted')
				return true
			})
		},
		deleteMember : (_, {id}) => {
			console.log(id)
			return Member.findByIdAndDelete(id, (err, member) => {
				if(!member){
					console.log('No Member Found')
					return false;
				}
				console.log('Member Destroyed!')
				return true;
			})
		},
		deleteTask : (_, {id}) => {
			return Task.findByIdAndDelete(id, (err, task) => {
				if(!task){
					console.log('No Task Found')
					return false
				}
				console.log('Task Destroyed!')
				return true;
			})
		},
		
		logInMember : (_, args) => {
			// console.log(args)
			return Member.findOne({ username: args.username }).then( member => {
				// console.log(user)
				if(member === null){
					console.log('User not found')
					return null
				}

				if(member.password === args.password){
					console.log('Welcome')
				}

				let hashedPassword = bcrypt.compareSync(args.password, member.password)
				console.log(hashedPassword)
				if(!hashedPassword){
					console.log('Wrong')
					return null
				}
				else{
					console.log('Welcome to the Server')
					console.log(member)
					return member
				}
			})
		}

	},

	// custom team type
	TeamType : {
		tasks : (_,args) => {
			// console.log(parent.id)
			return Task.find({ teamId: _.name })
		}
	},

	MemberType : {
		teams : (_, args) => {
			// console.log(_.id)
			return Team.findOne({ name: _.teamid})
		}
	},

	TaskType : {
		teams : (_, args) => {
			return Team.find({ name: _.teamId})
		}
	},

	ItemType : {
		teams : (_, args) => {
			return Team.find({ name: _.teamid})  // yung team name gagawing reference para hanapin sa parent which is yung kaparehas nyang name na teamid
		}
	}
}

// create an instance of the apollo server
// in the mose basic sense, the ApolloServer can be started
// By passing Schema type definitions(typeDefs) and the 
// resolvers responsible for fetching the data for the 
// requests/queries

const server = new ApolloServer({
	typeDefs,
	resolvers
});

module.exports = server;